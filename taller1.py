"""
Solución del Punto 1.
"""
Start = '111'
End   = '333'
limit = 0

graph = {
    
 '111': set (['211', '311']),
 '211': set (['231', '311', '111']),
 '311': set (['211', '321', '111']),
 '321': set (['221', '121', '311']),
 '231': set (['331', '131', '211']),
 '331': set (['131', '332', '231']),
 '131': set (['331', '231', '121']),
 '121': set (['221', '321', '131']),
 '221': set (['121', '223', '321']),
 '332': set (['132', '232', '331']),
 '132': set (['232', '332', '122']),
 '232': set (['132', '212', '332']),
 '122': set (['222', '322', '132']),
 '222': set (['322', '122']),
 '322': set (['222', '122', '312']),
 '212': set (['312', '112', '232']),
 '312': set (['112', '212', '322']),
 '112': set (['312', '212', '113']),
 '223': set (['323', '123', '221']),
 '123': set (['323', '223', '133']),
 '323': set (['123', '223', '313']),
 '133': set (['233', '333', '123']),
 '313': set (['113', '213', '323']),
 '113': set (['213', '313', '112']),
 '213': set (['113', '313', '233']),
 '233': set (['333', '133', '213']),
 '333': set (['233', '133']),
}


def Idfs_paths(graph, start, goal, max_lim):
    stack = [(start, [start])]
    while stack: #ciclo principal de búsqueda
        (vertex, path) = stack.pop()
        if(len(path) >= max_lim + 1):
            continue
        l = list(set(graph[vertex]) - set(path))
        for next in sorted(l, reverse=False):
                        
            if next == goal:
                return path + [next]
            else:
                stack.append((next, path + [next]))
    return None


for limite in range(len(graph)):

     print('Explorando con limite: %d' % limite)
     sol = Idfs_paths(graph, Start, End, limite)
     
     if sol == None:
         print ('Solucion no encontrada')
     else:
        print("Solucion Limite iterativo: ",sol, limite)
        break


def dfs_paths(graph, start, goal):
    stack = [(start, [start])]

    while stack:
        (vertex, path) = stack.pop()
        l = list(set(graph[vertex]) - set(path))
        for next in sorted(l, reverse=True):
                        
            if next == goal:
                yield path + [next]
            else:
                stack.append((next, path + [next]))

def bfs_paths(graph, start, goal):
    queue = [(start, [start])]
    while queue:
        (vertex, path) = queue.pop(0)
        l = list(set(graph[vertex]) - set(path))
        for next in sorted(l):
            if next == goal:
                 yield path + [next]
            else:
                queue.append((next, path + [next]))


print ("Solucion Profundidad: ",next(dfs_paths(graph, Start, End))) 
print ("Solucion Anchura: ", next(bfs_paths(graph, Start, End))) 
