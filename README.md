# Torres de Hannoi 

Se tienen tres torres y un conjunto de N discos de diferentes tamaños. Cada uno tiene una perforación en el centro que les permite deslizarse por las torres. Inicialmente, los N discos están ordenados de mayor a menor en una de las torres. Se deben pasar los discos a otra torre utilizando una intermedia como auxiliar. Los movimientos deben hacerse respetando las siguientes reglas: 1. En cada movimiento sólo puede desplazar un disco, 2. Solo se puede mover un disco si este se encuentra en el tope. 3. No puede quedar un disco sobre otro de menor tamaño.


Resolución del problema de la torres de hannoi utilizando los siguientes algoritmos.

### Primero en anchura

En Ciencias de la Computación, Búsqueda en anchura (en inglés BFS - Breadth First Search) es un algoritmo de búsqueda no informada utilizado para recorrer o buscar elementos en un grafo (usado frecuentemente sobre árboles). Intuitivamente, se comienza en la raíz (eligiendo algún nodo como elemento raíz en el caso de un grafo) y se exploran todos los vecinos de este nodo. A continuación para cada uno de los vecinos se exploran sus respectivos vecinos adyacentes, y así hasta que se recorra todo el árbol(Wikipedia).


### Primero en Profundidad

Una Búsqueda en profundidad (en inglés DFS o Depth First Search) es un algoritmo de búsqueda no informada utilizado para recorrer todos los nodos de un grafo o árbol (teoría de grafos) de manera ordenada, pero no uniforme. Su funcionamiento consiste en ir expandiendo todos y cada uno de los nodos que va localizando, de forma recurrente, en un camino concreto. Cuando ya no quedan más nodos que visitar en dicho camino, regresa (Backtracking), de modo que repite el mismo proceso con cada uno de los hermanos del nodo ya procesado(Wikipedia).

### Profundidad con limite iterativo

Una búsqueda en Profundidad Iterativa (BPI) es un algoritmo de búsqueda no informada utilizado para una estrategia de búsqueda en el espacio de estados en la que se realizan sucesivas búsquedas en profundidad limitada incrementando el límite de profundidad en cada iteración hasta alcanzar d, la profundidad del estado objetivo de menor profundidad. BPI es equivalente a la búsqueda en anchura, pero usa mucha menos memoria; en cada iteración, visita los nodos del árbol de búsqueda en el mismo orden que una búsqueda en profundidad, pero el orden en el que los nodos son visitados finalmente se corresponde con la búsqueda en anchura(Wikipedia).



### A asterisco

El problema de algunos algoritmos de búsqueda en grafos informados, como puede ser el algoritmo voraz, es que se guían en exclusiva por la función heurística, la cual puede no indicar el camino de coste más bajo, o por el coste real de desplazarse de un nodo a otro (como los algoritmos de escalada), pudiéndose dar el caso de que sea necesario realizar un movimiento de coste mayor para alcanzar la solución. Es por ello bastante intuitivo el hecho de que un buen algoritmo de búsqueda informada debería tener en cuenta ambos factores, el valor heurístico de los nodos y el coste real del recorrido(Wikipedia).

Heuristica para el caso de la torres de hannoi implementada es Distancia de Hamming(https://es.wikipedia.org/wiki/Distancia_de_Hamming).

https://github.com/chiho828/State-Space-Search - Repositorio de apoyo